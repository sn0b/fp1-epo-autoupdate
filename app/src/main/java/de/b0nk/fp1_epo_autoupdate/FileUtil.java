/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.util.Log;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;
import com.stericson.RootTools.execution.CommandCapture;
import com.stericson.RootTools.execution.Shell;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class FileUtil {

    // TODO clean up and improve

    public static String ls(final String file) throws TimeoutException, RootDeniedException, IOException {

        CommandCapture command = new CommandCapture(0, "ls -l " + file) {
            @Override
            public void commandOutput(int id, String line) {
                super.commandOutput(id, line);

//                Log.d(Settings.LOG_DEBUG_TAG, "id: " + id + ", line: " + line);
            }
        };

        Shell shell = RootTools.getShell(false);
        shell.add(command);

        while (!command.isFinished()) {
            Log.d(Settings.LOG_DEBUG_TAG, "Waiting for command to complete...");
            try { Thread.sleep(50); } catch (InterruptedException e) { /* ignore */ }
        }

        shell.close();
        Log.d(Settings.LOG_DEBUG_TAG, "Command completed, shell closed.");

        String output = command.toString();

        return output.substring(0, output.length() - 1);
    }

    public static String getUserAndGroup(final String file) throws TimeoutException, RootDeniedException, IOException {

        String lsOutput = ls(file);

        // TODO compile pattern and use matcher
        // '-rw------- system   system     276480 2014-09-16 20:19 EPO.DAT'
//        if (lsOutput.matches("\\-([r\\-][w\\-][x\\-]){3}\\s\\w{1,12}\\s+\\w{1,12}\\s+.*EPO\\.DAT"
//              + System.getProperty("line.separator"))) {
        if (lsOutput.matches("\\-([r\\-][w\\-][x\\-]){3}\\s\\w{1,12}\\s+\\w{1,12}\\s+.*EPO\\.DAT")) {

            int fws = lsOutput.indexOf(' ');
            int sws = lsOutput.indexOf(' ', fws + 1);
            Log.d(Settings.LOG_DEBUG_TAG, "fws: " + fws + ", sws: " + sws);

            String user = lsOutput.substring(fws, sws).trim();
            Log.d(Settings.LOG_DEBUG_TAG, "user: " + user);

            while (lsOutput.charAt(sws) == ' ') {
                sws++;
            }
            int tws = lsOutput.indexOf(' ', sws);
            Log.d(Settings.LOG_DEBUG_TAG, "sws: " + sws + ", tws: " + tws);

            String group = lsOutput.substring(sws, tws).trim();
            Log.d(Settings.LOG_DEBUG_TAG, "group: " + group);

            return user + ":" + group;
        }
        // TODO check if it's required to match for different busybox, toolbox etc. versions
        // '-rw-rw-r--    1 gps      9997        276480 Sep 16 21:52 /data/misc/EPO.DAT'
/*        else if (lsOutput.matches("\\-([r\\-][w\\-][x\\-]){3}\\s+\\d{1,3}\\s\\w{1,12}\\s+\\w{1,12}\\s+.*EPO\\.DAT")) {

            Log.d(Settings.LOG_DEBUG_TAG, "Output matches busybox.");

            int fws = lsOutput.indexOf(' ');
            while (lsOutput.charAt(fws) == ' ') {
                fws++;
            }
            fws = lsOutput.indexOf(' ', fws);
            int sws = lsOutput.indexOf(' ', fws);
            Log.d(Settings.LOG_DEBUG_TAG, "fws: " + fws + ", sws: " + sws);

            String user = lsOutput.substring(fws, sws).trim();
            Log.d(Settings.LOG_DEBUG_TAG, "user: " + user);

            while (lsOutput.charAt(sws) == ' ') {
                sws++;
            }
            int tws = lsOutput.indexOf(' ', sws);
            Log.d(Settings.LOG_DEBUG_TAG, "sws: " + sws + ", tws: " + tws);

            String group = lsOutput.substring(sws, tws).trim();
            Log.d(Settings.LOG_DEBUG_TAG, "group: " + group);

            return user + ":" + group;
        }*/
        else {
            Log.d(Settings.LOG_DEBUG_TAG, "Invalid output.");
            return null;
        }
    }

    public static void setUserAndGroup(final String file, final String userAndGroup) throws TimeoutException, RootDeniedException, IOException {
        CommandCapture command = new CommandCapture(0, "chown " + userAndGroup + " " + file) {
            @Override
            public void commandOutput(int id, String line) {
                super.commandOutput(id, line);

                Log.d(Settings.LOG_DEBUG_TAG, "id: " + id + ", line: " + line);
            }
        };

        Shell shell = RootTools.getShell(true);
        shell.add(command);

        while (!command.isFinished()) {
            Log.d(Settings.LOG_DEBUG_TAG, "Waiting for command to complete...");
            try { Thread.sleep(50); } catch (InterruptedException e) { /* ignore */ }
        }

        shell.close();
        Log.d(Settings.LOG_DEBUG_TAG, "Command completed, shell closed.");
    }

    public static void setPermissions(final String file, final String permissions) throws TimeoutException, RootDeniedException, IOException {
        CommandCapture command = new CommandCapture(0, "chmod " + permissions + " " + file) {
            @Override
            public void commandOutput(int id, String line) {
                super.commandOutput(id, line);

                Log.d(Settings.LOG_DEBUG_TAG, "id: " + id + ", line: " + line);
            }
        };

        Shell shell = RootTools.getShell(true);
        shell.add(command);

        while (!command.isFinished()) {
            Log.d(Settings.LOG_DEBUG_TAG, "Waiting for command to complete...");
            try { Thread.sleep(50); } catch (InterruptedException e) { /* ignore */ }
        }

        shell.close();
        Log.d(Settings.LOG_DEBUG_TAG, "Command completed, shell closed.");
    }
}
