/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.exceptions.RootDeniedException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeoutException;

public class HttpFetcher extends AsyncTask<Object, String, String> {

    private static final Semaphore lock = new Semaphore(1);

    private Context context = null;

    private ScrollView scrollView = null;
    private TextView resultView = null;

    @Override
    protected String doInBackground(Object ... params) {

        // TODO check if cancellation works

        context = (Context) params[0];
        resultView = (TextView) params[1];
        scrollView = (ScrollView) params[2];
        boolean forceDownload = (Boolean) params[3];

        if (resultView != null) {
            Log.d(Settings.LOG_DEBUG_TAG, "Using resultView for logging...");
        }
        else {
            Log.d(Settings.LOG_DEBUG_TAG, "Using notifications for logging...");
        }

        SharedPreferences settings = context.getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);
        long updateInterval = Long.parseLong(settings.getString(Settings.COMMON_UPDATE_INTERVAL_KEY,
                Settings.COMMON_UPDATE_INTERVAL_DEFAULT)) * 60 * 1000;  // to millis
        long lastUpdate = Long.parseLong(settings.getString(Settings.COMMON_LAST_UPDATE_KEY,
                Settings.COMMON_LAST_UPDATE_DEFAULT));
        long now = System.currentTimeMillis();

        String server = settings.getString(Settings.HTTP_SERVER_KEY,Settings.HTTP_SERVER_DEFAULT);
        String port = settings.getString(Settings.HTTP_PORT_KEY, Settings.HTTP_PORT_DEFAULT);
        String path = settings.getString(Settings.HTTP_PATH_KEY, Settings.HTTP_PATH_DEFAULT);

        Log.d(Settings.LOG_DEBUG_TAG, "lastUpdate: " + lastUpdate + ", now: " + now
                + ", (now - lastUpdate): " + (now - lastUpdate) + ", updateInterval: " + updateInterval);

        if (now - lastUpdate < updateInterval && !forceDownload) {
            return "Successful: Within update interval, update skipped...";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(server);
        if (!port.equals("")) {
            sb.append(":");
            sb.append(port);
        }
        sb.append(path);
        String url = sb.toString();

        HttpURLConnection connection = null;
        BufferedInputStream bis = null;
        FileOutputStream fos = null;
        File tempFile = null;

        try {
            if (lock.tryAcquire()) {
                //Log.d(Settings.LOG_DEBUG_TAG, "Connecting: " + url);
                publishProgress("Connecting: " + url);

                connection = (HttpURLConnection) new URL(url).openConnection();

                File currentFile = new File("/data/misc/EPO.DAT");
                Log.d(Settings.LOG_DEBUG_TAG, "Last modified: local: " + new Date(currentFile.lastModified())
                        + ", remote: " + new Date(connection.getLastModified()));

                if (currentFile.lastModified() >= connection.getLastModified() && !forceDownload) {
                    return "Successful: Current file has same date or newer.";
                }

                if (isCancelled()) {
                    return "Canceled.";
                }

                bis = new BufferedInputStream(connection.getInputStream());
                int bytesToRead = connection.getContentLength();
                int bytesRead = 0;

                Log.d(Settings.LOG_DEBUG_TAG, "Downloading " + bytesToRead + " bytes...");
                publishProgress("Downloading " + bytesToRead + " bytes...");

                tempFile = File.createTempFile(currentFile.getName(), null);    // EPO.DAT
                fos = new FileOutputStream(tempFile);
                byte[] buffer = new byte[4096];
                int bytesAvailable;

                while ((bytesAvailable = bis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesAvailable);
                    bytesRead += bytesAvailable;

                    //Log.d(Settings.LOG_DEBUG_TAG, "Read " + bytesRead + " of " + bytesToRead + " bytes.");
                    publishProgress("Read " + bytesRead + " of " + bytesToRead + " bytes.");

                    if (isCancelled()) {
                        return "Canceled.";
                    }
                }

                fos.flush();
                if (bytesRead != bytesToRead) {
                    return "Download incomplete";
                }

                //Log.d(Settings.LOG_DEBUG_TAG, bytesRead + " of " + bytesToRead + " bytes read, download finished.");
                publishProgress(bytesRead + " of " + bytesToRead + " bytes read, download finished.");

                if (RootTools.isAccessGiven()) {

                    //Log.d(Settings.LOG_DEBUG_TAG, "File info before copy: '" + FileUtil.ls(currentFile.getPath()) + "'");
                    publishProgress("File info before copy:" + System.getProperty("line.separator") + FileUtil.ls(currentFile.getPath()));

                    String userAndGroup = null;

                    if (Build.MODEL.equals("ALCATEL ONE TOUCH 997D")) {
                        userAndGroup = FileUtil.getUserAndGroup(currentFile.getPath()); // '/data/misc/EPO.DAT'

                        if (userAndGroup == null) {
                            return "Couldn't detect user and group.";
                        }

                        //Log.d(Settings.LOG_DEBUG_TAG, "Detected user and group: " + userAndGroup);
                        publishProgress("Detected user and group: " + userAndGroup);
                    }

                    // copy file
                    if (RootTools.copyFile(tempFile.getAbsolutePath(), currentFile.getAbsolutePath(), true, false)) {

                        //Log.d(Settings.LOG_DEBUG_TAG, "File info after copy: '" + FileUtil.ls(currentFile.getPath()) + "'");
                        publishProgress("File info after copy:" + System.getProperty("line.separator") + FileUtil.ls(currentFile.getPath()));

                        if (Build.MODEL.equals("ALCATEL ONE TOUCH 997D")) {
                            FileUtil.setUserAndGroup(currentFile.getPath(), userAndGroup);  // '/data/misc/EPO.DAT'

                            //Log.d(Settings.LOG_DEBUG_TAG, "File info after adjustment: '" + FileUtil.ls(currentFile.getPath()) + "'");
                            publishProgress("File info after adjustment:" + System.getProperty("line.separator") + FileUtil.ls(currentFile.getPath()));

                            //Log.d(Settings.LOG_DEBUG_TAG, "File copied, permissions adjusted.");
                            publishProgress("File copied, permissions adjusted.");
                        }
                        else {
                            // adjust permissions even on Fairphone (maybe the user messed it up manually before
                            // using FP1-EPO-Autoupdate)
                            FileUtil.setUserAndGroup(currentFile.getPath(), "0:0");  // set root:root on '/data/misc/EPO.DAT'
                            FileUtil.setPermissions(currentFile.getPath(), "644");  // set rw-r--r-- on '/data/misc/EPO.DAT'

                            //Log.d(Settings.LOG_DEBUG_TAG, "File info after adjustment: '" + FileUtil.ls(currentFile.getPath()) + "'");
                            publishProgress("File info after adjustment:" + System.getProperty("line.separator") + FileUtil.ls(currentFile.getPath()));

                            //Log.d(Settings.LOG_DEBUG_TAG, "File copied, permissions adjusted.");
                            publishProgress("File copied, permissions adjusted.");

                            //Log.d(Settings.LOG_DEBUG_TAG, "File copied.");
                            //publishProgress("File copied.");
                        }
                    }
                    else {
                        return "Couldn't copy file.";
                    }
                }
                else {
                    return "Root access denied.";
                }

                SharedPreferences.Editor editor = settings.edit();
                editor.putString(Settings.COMMON_LAST_UPDATE_KEY, Long.toString(currentFile.lastModified()));
                editor.commit();

                Log.d(Settings.LOG_DEBUG_TAG, "Last update time saved.");
                publishProgress("Last update time saved.");
            }
            else {
                Log.d(Settings.LOG_DEBUG_TAG, "Another instance is already running, aborting...");
                publishProgress("Another instance is already running, aborting...");
            }
        }
        catch (MalformedURLException e) {
            return e.getMessage();
        }
        catch (IOException e) {
            return e.getMessage();
        }
        catch (TimeoutException e) {
            return "Root access timed out.";
        }
        catch (RootDeniedException e) {
            return "Root access denied.";
        }
        finally {
            Log.d(Settings.LOG_DEBUG_TAG, "Freeing resources...");

            if (bis != null) {
                try {
                    bis.close();
                }
                catch (IOException e) { /* ignore */ }
            }

            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) { /* ignore */ }
            }

            if (tempFile != null) {
                tempFile.delete();
            }

            if (connection != null) {
                connection.disconnect();
            }

            lock.release();
        }

        return "Successful.";
    }

    @Override
    protected void onProgressUpdate(String ... values) {

        // TODO really stack notifications...

        for (String value : values) {
            Log.d(Settings.LOG_DEBUG_TAG, value);

            if (resultView != null) {
                resultView.append(value + System.getProperty("line.separator"));
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
            else {
                NotificationUtil.showInfo(context, value);
            }
        }
    }

    @Override
    protected void onPostExecute(String result) {

        // TODO ...and remove them on success

        Log.d(Settings.LOG_DEBUG_TAG, result);

        if (resultView != null) {
            resultView.append(result + System.getProperty("line.separator"));
            scrollView.fullScroll(View.FOCUS_DOWN);
        }
        else {
            if (!result.startsWith("Successful")) {
                NotificationUtil.showError(context, result);
            }

            NotificationUtil.clear(context);
        }
    }
}
