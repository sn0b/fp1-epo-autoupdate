/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.stericson.RootTools.RootTools;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Activity {

    private SharedPreferences settings = null;
    private Timer statusUpdater = null;
    private boolean settingsSaved = true;

    // TODO apply some fixes from http://forum.xda-developers.com/showthread.php?t=2793470
    // TODO and from http://forum.xda-developers.com/wiki/Fairphone_Fairphone/Guides
    // TODO and http://jtgeek.com/tuto-injecter-les-fichiers-gps-pour-la-france-sur-smartphone-chinois/
    // TODO automatically

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO add path to EPO.DAT on device and add user/group along with permissions

        settings = getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);

        setStringValue(R.id.http_server, settings.getString(Settings.HTTP_SERVER_KEY,Settings.HTTP_SERVER_DEFAULT));
        setStringValue(R.id.http_port, settings.getString(Settings.HTTP_PORT_KEY, Settings.HTTP_PORT_DEFAULT));
        setStringValue(R.id.http_path, settings.getString(Settings.HTTP_PATH_KEY, Settings.HTTP_PATH_DEFAULT));

        setStringValue(R.id.common_update_interval, settings.getString(Settings.COMMON_UPDATE_INTERVAL_KEY,
                Settings.COMMON_UPDATE_INTERVAL_DEFAULT));

        // TODO replace with drop down -> WIFI, MOBILE, ROAMING (or ALWAYS)
        setBooleanValue(R.id.common_wifi_only, settings.getString(Settings.COMMON_WIFI_ONLY_KEY,
                Settings.COMMON_WIFI_ONLY_DEFAULT));

        Log.d(Settings.LOG_DEBUG_TAG, "Settings initialized.");

        statusUpdater = new Timer();
        statusUpdater.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showStatus();
                    }
                });
           }
        }, Settings.STATUS_UPDATE_INTERVAL, Settings.STATUS_UPDATE_INTERVAL);
    }

    // very cool, examples here: http://techblogon.com/android-screen-orientation-change-rotation-example/
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(Settings.LOG_DEBUG_TAG, "Received event for: orientation/screenSize/keyboardHidden");
    }

    public void save(View view) {

        Log.d(Settings.LOG_DEBUG_TAG, "Saving settings...");
        SharedPreferences.Editor editor = settings.edit();

        String httpPort = getStringValue(R.id.http_port);
        if (!httpPort.equals("")) {
            long port = Long.parseLong(httpPort);
            if (port < 1 || 65535 < port) {
                NotificationUtil.showToast(this, "Port must be >= 1 and <= 65535.");
                EditText t = (EditText) findViewById(R.id.http_port);
                t.requestFocus();
                return;
            }
        }

        String updateInterval = getStringValue(R.id.common_update_interval);
        if (updateInterval.equals("") || Long.parseLong(updateInterval) < 1440
                || 43200 < Long.parseLong(updateInterval)) {   // 1440 = 1 day, 43200 = 30 days (in minutes)

            NotificationUtil.showToast(this, "Update interval must be >= 1440 and <= 43200.");
            EditText t = (EditText) findViewById(R.id.common_update_interval);
            t.requestFocus();
            return;
        }

        String httpServer = getStringValue(R.id.http_server);
        String httpPath = getStringValue(R.id.http_path);

        StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(httpServer);
        if (!httpPort.equals("")) {
            sb.append(":");
            sb.append(httpPort);
        }
        sb.append(httpPath);

        try {
            // TODO make regex's for port and path
            URL testUrl = new URL(sb.toString());
            Log.d(Settings.LOG_DEBUG_TAG, "URL: " + testUrl);
        }
        catch (MalformedURLException e) {
            NotificationUtil.showToast(this, "Either server or path are invalid.");
            EditText t = (EditText) findViewById(R.id.http_server);
            t.requestFocus();
            return;
        }

        // HTTP
        editor.putString(Settings.HTTP_SERVER_KEY, httpServer);
        editor.putString(Settings.HTTP_PORT_KEY, httpPort);
        editor.putString(Settings.HTTP_PATH_KEY, httpPath);

        // COMMON
        editor.putString(Settings.COMMON_UPDATE_INTERVAL_KEY, updateInterval);
        editor.putString(Settings.COMMON_WIFI_ONLY_KEY, getBooleanValue(R.id.common_wifi_only));

        editor.commit();

        settingsSaved = true;

        NotificationUtil.showToast(this, "Settings saved.");
    }

    public void enable(View view) {

        Log.d(Settings.LOG_DEBUG_TAG, "Enabling autoupdate.");

        // TODO on first run, runs this?
        // save(view);

        if (!RootTools.isRootAvailable()) {
            NotificationUtil.showToast(this, "You need to allow permanent root access for automatic updates to work.");
            return;
        }

        getPackageManager().setComponentEnabledSetting(new ComponentName(this, ConnectionOrAlarmEventsReceiver.class),
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, ConnectionOrAlarmEventsReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

        long updateInterval = Long.parseLong(getStringValue(R.id.common_update_interval)) * 60 * 1000;

        Log.d(Settings.LOG_DEBUG_TAG, "Scheduling: updateInterval: " + updateInterval);

        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, 0, updateInterval, pendingIntent);

        NotificationUtil.showToast(this, "Autoupdate enabled.");
    }

    /** Called when the user clicks the 'Disable' button */
    public void disable(View view) {

        Log.d(Settings.LOG_DEBUG_TAG, "Disabling autoupdate.");

        getPackageManager().setComponentEnabledSetting(new ComponentName(this, ConnectionOrAlarmEventsReceiver.class),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, ConnectionOrAlarmEventsReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        alarmManager.cancel(pendingIntent);

        NotificationUtil.showToast(this, "Autoupdate disabled.");
    }

    /** Called when the user clicks the 'Force Update' button */
    public void forceUpdate(View view) {
        Log.d(Settings.LOG_DEBUG_TAG, "Forcing update...");

        if (!RootTools.isRootAvailable()) {
            NotificationUtil.showToast(this, "You need to allow root access for the download to work.");
            return;
        }

        Intent intent = new Intent(this, ShowResultActivity.class);
        startActivity(intent);
    }

    public void showLog(View view) {
        //Log.d(Settings.LOG_DEBUG_TAG, "Showing log...");

        Intent intent = new Intent(this, ShowLogActivity.class);
        startActivity(intent);
    }

    public void about(View view) {
        Log.d(Settings.LOG_DEBUG_TAG, "Showing license...");

        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void setStringValue(final int id, final String value) {
        EditText t = (EditText) findViewById(id);
        t.setText(value);
        t.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable editable) {
                settingsSaved = false;
                showStatus();
            }
        });
    }

    private void setBooleanValue(final int id, final String value) {
        CheckBox b = (CheckBox) findViewById(id);
        b.setChecked(Boolean.valueOf(value));
    }

    private String getStringValue(final int resourceId) {
        EditText t = (EditText) findViewById(resourceId);
        return t.getText().toString();
    }

    private String getBooleanValue(final int resourceId) {
        CheckBox b = (CheckBox) findViewById(resourceId);
        return Boolean.toString(b.isChecked());
    }

    private void showStatus() {
        TextView statusView = (TextView) findViewById(R.id.current_status);
        int status = getPackageManager().getComponentEnabledSetting(new ComponentName(this,
                ConnectionOrAlarmEventsReceiver.class));

        Date lastUpdate = new Date(Long.parseLong(settings.getString(Settings.COMMON_LAST_UPDATE_KEY,
                Settings.COMMON_LAST_UPDATE_DEFAULT)));

        if (status == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) {
            statusView.setBackgroundColor(Color.GREEN);
            statusView.setTextColor(Color.BLACK);
            statusView.setText("Status: Active :-)" + System.getProperty("line.separator") + "Last Update: " + lastUpdate);
        }
        else {
            statusView.setBackgroundColor(Color.RED);
            statusView.setTextColor(Color.WHITE);
            statusView.setText("Status: Disabled :-(" + System.getProperty("line.separator") + "Last Update: " + lastUpdate);
        }

        if (!settingsSaved) {
            statusView.append(System.getProperty("line.separator") + "Settings: Unsaved");
        }
    }
}
