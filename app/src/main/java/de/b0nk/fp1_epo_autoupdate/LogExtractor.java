/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogExtractor {
    public static String getRecentLogs(final Context context) throws IOException, InterruptedException {
        File logFile = null;
        BufferedReader br = null;
        try {
/*            int result = Runtime.getRuntime().exec("pm grant " + LogExtractor.class.getPackage()
                    + " android.permission.READ_LOGS").waitFor();
            Log.d(LogExtractor.class.getSimpleName(), "Result: " + result);
            NotificationUtil.showToast(context, "Result: " + result);*/

            Log.d(LogExtractor.class.getSimpleName(), "Creating temporary file and executing logcat...");
            logFile = File.createTempFile(context.getString(R.string.app_name) + "-log-", ".tmp");
            logFile.deleteOnExit();
            int result = Runtime.getRuntime().exec("logcat -t 300 -f " + logFile.getAbsolutePath() + " -s " +
                    Settings.LOG_DEBUG_TAG + ":D").waitFor();

            Log.d(LogExtractor.class.getSimpleName(), "Result: " + result + ", reading output...");
            NotificationUtil.showToast(context, "Result: " + result + ", reading output...");

            br = new BufferedReader(new FileReader(logFile));
            StringBuilder log = new StringBuilder();

            String line = br.readLine();
            log.append(line);   // '--------- beginning of /dev/log/main'
            log.append(System.getProperty("line.separator"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

            while ( (line = br.readLine()) != null) {

                // + 2 to remove the 'D/' prefix, too
                //log.append(line.substring(Settings.LOG_DEBUG_TAG.length() + 2));

                int pos;
                if ( (pos = line.indexOf(")")) > -1) {
                    log.append("(");
                    log.append(sdf.format(new Date()));
                    log.append(line.substring(pos));
                    log.append(System.getProperty("line.separator"));
                }

                Log.d(LogExtractor.class.getSimpleName(), "Added line: " + line);
            }

            Log.d(LogExtractor.class.getSimpleName(), "Full log is: " + log.toString());
            //NotificationUtil.showToast(context, "Full log is: " + log.toString());
            return log.toString();
        }
        catch (IOException e) {
            throw e;
        }
        catch (InterruptedException e) {
            throw e;
        }
        finally {
            if (br != null) {
                try { br.close(); } catch (IOException e) { /* ignored */ }
            }

            if (logFile != null) {
                logFile.delete();
            }
        }
    }
}
