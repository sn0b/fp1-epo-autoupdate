/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ConnectionOrAlarmEventsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d(Settings.LOG_DEBUG_TAG, "onReceive()");

        SharedPreferences settings = context.getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);

        if (intent.getAction() == null) {
            Log.d(Settings.LOG_DEBUG_TAG, "Triggered by alarm event.");
//            NotificationUtil.showInfo(context, "Checking...");
        }
        else if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            Log.d(Settings.LOG_DEBUG_TAG, "Triggered by network event.");
//            NotificationUtil.showInfo(context, "Checking...");
        }
        else if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.d(Settings.LOG_DEBUG_TAG, "Triggered by reboot event.");

            // broadcast can be simulated like this:
            // adb shell
            // am broadcast -a android.intent.action.BOOT_COMPLETED
            // however, this does not behave like a real reboot!

            // adb logcat|grep -E 'ConnectionOrAlarmEventsReceiver|HttpFetcher|ShowResultActivity
            // helps a lot in debugging

            // TODO check why neither notifications nor toasts seem to work in this stage
//            NotificationUtil.showInfo(context, "Triggered by reboot event.");
//            Toast.makeText(context, "Triggered by reboot event.", Toast.LENGTH_SHORT).showInfo();

            // TODO check when and how this can work
            //android.os.Debug.waitForDebugger();

            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent alarmIntent = new Intent(context, ConnectionOrAlarmEventsReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);

            long updateInterval = Long.parseLong(settings.getString(Settings.COMMON_UPDATE_INTERVAL_KEY,
                    Settings.COMMON_UPDATE_INTERVAL_DEFAULT)) * 60 * 1000;

            Log.d(Settings.LOG_DEBUG_TAG, "Scheduling: updateInterval: " + updateInterval);

            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, 0, updateInterval, pendingIntent);

            Log.d(Settings.LOG_DEBUG_TAG, "Alarm rescheduled.");

            return;
        }

        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            //Log.d(Settings.LOG_DEBUG_TAG, "Network is/has been connected.");
            NotificationUtil.showInfo(context, "Checking...");

            boolean wifiOnly = Boolean.parseBoolean(settings.getString(Settings.COMMON_WIFI_ONLY_KEY,
                    Settings.COMMON_WIFI_ONLY_DEFAULT));

            if (networkInfo.isRoaming()) {
                Log.d(Settings.LOG_DEBUG_TAG, "Network is roaming, aborting...");
                NotificationUtil.showInfo(context, "Network is roaming, aborting...");
            }
            else if (wifiOnly && networkInfo.getType() != ConnectivityManager.TYPE_WIFI) {
                Log.d(Settings.LOG_DEBUG_TAG, "Not a WiFi network, aborting...");
                NotificationUtil.showInfo(context, "Not a WiFi network, aborting...");
            }
            else {
                new HttpFetcher().execute(context, null, null, false);
            }
        }
        else {

            //NotificationUtil.showInfo(context, "Checking...");

            long lastUpdate = Long.parseLong(settings.getString(Settings.COMMON_LAST_UPDATE_KEY,
                    Settings.COMMON_LAST_UPDATE_DEFAULT));
            long now = System.currentTimeMillis();

            Log.d(Settings.LOG_DEBUG_TAG, "lastUpdate: " + lastUpdate + ", Settings.OUTDATED_INTERVAL: "
                    + Settings.OUTDATED_INTERVAL + ", lastUpdate + Settings.OUTDATED_INTERVAL: "
                    + (lastUpdate + Settings.OUTDATED_INTERVAL) + ", now: " + now);

            if (lastUpdate + Settings.OUTDATED_INTERVAL <= now) {
                Log.d(Settings.LOG_DEBUG_TAG, "File is outdated. Please active internet access to allow an update.");
                NotificationUtil.showError(context, "File is outdated. Please active internet access to allow an update.");
            }
        }

        NotificationUtil.clear(context);
    }
}
