/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class NotificationUtil {

    public static final int NOTIFICATION_INFO_ID = 1000;
    public static final int NOTIFICATION_ERROR_ID = 1001;

    public static Timer clearNotificationsTimer = null;

    public static void showInfo(final Context context, final String msg) {

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.drawable.ic_notify_progress);
        builder.setContentTitle(context.getString(R.string.app_name));

        // https://stackoverflow.com/questions/16179899/android-using-auto-cancel-on-a-notification-when-your-app-is-running-in-the-bac
        builder.setAutoCancel(true);
        PendingIntent backToCurrentlyRunningAppIntent =
                PendingIntent.getActivity(context.getApplicationContext(), 0, new Intent(), 0);
        builder.setContentIntent(backToCurrentlyRunningAppIntent);

        builder.setContentText(msg);
        notificationManager.notify(NOTIFICATION_INFO_ID, builder.build());
    }

    public static void showError(final Context context, final String msg) {

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setSmallIcon(R.drawable.ic_notify_progress);
        builder.setContentTitle(context.getString(R.string.app_name));

        // https://stackoverflow.com/questions/16179899/android-using-auto-cancel-on-a-notification-when-your-app-is-running-in-the-bac
        builder.setAutoCancel(true);
        PendingIntent backToCurrentlyRunningAppIntent =
                PendingIntent.getActivity(context.getApplicationContext(), 0, new Intent(), 0);
        builder.setContentIntent(backToCurrentlyRunningAppIntent);

        builder.setContentText(msg);
        notificationManager.notify(NOTIFICATION_ERROR_ID, builder.build());
    }

    public static synchronized void clear(final Context context) {
        if (clearNotificationsTimer != null) {
            clearNotificationsTimer.cancel();
        }

        clearNotificationsTimer = new Timer();
        clearNotificationsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                NotificationManager notificationManager =
                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.cancel(NOTIFICATION_INFO_ID);
            }
        }, Settings.CLEAR_NOTIFICATIONS_TIMEOUT);
    }

    public static void showToast(final Context context, final String msg) {
        Log.d(Settings.LOG_DEBUG_TAG, msg);

        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}
