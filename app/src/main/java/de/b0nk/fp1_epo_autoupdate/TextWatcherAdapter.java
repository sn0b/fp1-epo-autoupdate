/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.text.TextWatcher;
import android.util.Log;

public abstract class TextWatcherAdapter implements TextWatcher {

    protected String oldValue = null;
    protected String newValue = null;

    @Override
    public void beforeTextChanged(CharSequence charSequence,  int start, int count, int after) {
        oldValue = charSequence.toString();
        Log.d(Settings.LOG_DEBUG_TAG, "oldValue: " + oldValue);
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        newValue = charSequence.toString();
        Log.d(Settings.LOG_DEBUG_TAG, "newValue: " + newValue);
    }
}
