/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;

public class ShowLogActivity extends Activity {

    private TextView logView = null;
    private ScrollView scrollView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_log);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        logView = (TextView) findViewById(R.id.logView);

        try {
            logView.setText(LogExtractor.getRecentLogs(this));
        }
        catch (IOException e) {
            Log.d(Settings.LOG_DEBUG_TAG, "Couldn't read log: " + e.getMessage());
            logView.setText("Couldn't read log: " + e.getMessage());
        }
        catch (InterruptedException e) {
            Log.d(Settings.LOG_DEBUG_TAG, "Couldn't read log: " + e.getMessage());
            logView.setText("Couldn't read log: " + e.getMessage());
        }
    }

    // very cool, examples here: http://techblogon.com/android-screen-orientation-change-rotation-example/
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(Settings.LOG_DEBUG_TAG, "Received event for: orientation/screenSize/keyboardHidden");
        scrollView.fullScroll(View.FOCUS_DOWN);
    }
}
