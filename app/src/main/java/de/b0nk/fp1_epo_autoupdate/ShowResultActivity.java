/*
 * Copyright (C) 2014 Ragnar Bonk, fp1-epo-autoupdate@b0nk.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.b0nk.fp1_epo_autoupdate;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

public class ShowResultActivity extends Activity {

    private TextView resultView = null;
    private ScrollView scrollView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        resultView = (TextView) findViewById(R.id.resultView);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            SharedPreferences settings = getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);
            boolean wifiOnly = Boolean.parseBoolean(settings.getString(Settings.COMMON_WIFI_ONLY_KEY,
                    Settings.COMMON_WIFI_ONLY_DEFAULT));

            if (networkInfo.isRoaming()) {
                writeToResultView("Network is roaming, aborting...");
            }
            else if (wifiOnly && networkInfo.getType() != ConnectivityManager.TYPE_WIFI) {
                writeToResultView("Not a WiFi network, aborting...");
            }
            else {
                new HttpFetcher().execute(this, resultView, scrollView, true);
            }
        }
        else {
            writeToResultView("No internet connection available.");
        }
    }

    // very cool, examples here: http://techblogon.com/android-screen-orientation-change-rotation-example/
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(Settings.LOG_DEBUG_TAG, "Received event for: orientation/screenSize/keyboardHidden");
        scrollView.fullScroll(View.FOCUS_DOWN);
    }

    private void writeToResultView(final String msg) {
        resultView.append(msg + System.getProperty("line.separator"));
        scrollView.fullScroll(View.FOCUS_DOWN);
    }
}
